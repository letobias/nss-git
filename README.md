# ${CI_PROJECT_NAME}

## Seznam Autorů

- Já

## Seznam Použitých Technologií

- Haskell
- JavaScript
- PHP

## Popis

Tato aplikace je určena k ničemu

## Název Instalace

Pro instalaci aplikace postupujte následovně:

1. Stáhni repozitáře na lokální stroj.
2. Otevři terminál a naviguj do složky s projektem.
3. Spusť příkazu `npm install`.
4. Nastav konfiguračních souborů podle potřeby.
5. Spusť aplikace příslušným příkazem.

## Copyright

© 2024 Tobias Le
